# Proyecto del Curso Tópicos 2

## Descripción del Proyecto
Hoy en día hay muchos emprendedores que tratan de obtener ingresos a partir de 
sus ideas de negocio y obtener buenos resultados para así lograr independizarse
y por supuesto que en busca de un mejor pasar. En el mundo global y las relaciones 
comerciales que mantiene Chile con el resto del mundo, las exportaciones de 
diferentes productos han dado buenos resultados por sus precios competitivos.
En esta ocasión una mini pyme del área tecnológica nos ha pedido una solución
para tener un mejor manejo y control de inventarios de los productos que almacenan en su 
bodega

Nuestra empresa implementará una solución a través del framework Django que permita
resolver su problemática.

## Nuestra solución contempla las siguientes funcionalidades requeridas por nuestro cliente

* La solución debe permitir agregar tres tipos de productos: Notebooks, Desktops y Celulares
con la opción de adjuntar la foto del producto y desplegarla tanto en el navegador como 
en el administrador de Django

* Se debe poder editar o eliminar productos

* Los productos deben tener la información de modelo, precio, estado y detalle  

* Sólo los usuarios con el rol de administrador prodrán eliminar productos

* El sistema cuenta con dos tipos de usuario y sus características se describen a continuación:

* Perfil Administrador:
Este usuario cuanta con todos los permisos para tanto para crear,
editar, eliminar. A los Users, desktops, Laptops y Mobiles.

* Perfil Usuario:
* El perfil cuenta con los siguiente permisos:
* El usuario puede editar el estado de los productos.
* El usuario puede agregar productos.
* El usuario tiene a todas las vistas del diferentes productos.


## Se consideraron los siguientes requrimientos no funcionales en el proyecto:

* El sistema debe poseer al menos 4 entidades a modelar y deben ser modeladas
con Django

* Cada entidad debe tener implementado la funcionalidad de agregar, eliminar,
actualizar y listar (CRUD)

* Cada operación del CRUD debe soportar el sistema de permisos de Django y solo
podrá ser accedido por los permisos que tenga el usuario

* El sistema de gestionar autenticación de usuarios a través del sistema de 
autenticación que proporciona Django

* El sistema debe manejar grupos de usuarios con al menos 2 roles

* Debe proveer una funcionalidad para subir archivos (foto o documentos)

* Debe proporcionar una funcionalidad para gestionar el perfil de usuario
autenticado

##Repository: https://gitlab.com/mandinga27/django-project-2
* Clonar desde gitlab
* Ejecutar "docket-compose up"
* Abrir en el navegador "http://localhost:8000/admin/"

##Ingresar con:
* User superAdmin: root
* Password: coyote3014

##Para probar produccion: 
* Abrir en el navegador "http://localhost:8000"

# Usuario 1:
* User: test1
* password: coyote3014

<------------------------->

Tópicos de desarrollo WEB
Profesor: Miguel Cantillana
Alumnos: 
Matias Arriagada
Abraham Musa
