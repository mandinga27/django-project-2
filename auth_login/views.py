from django.shortcuts import render, redirect
from django.contrib import auth #authenticate, login, logout
from django.contrib import messages
"""
from auth_login.forms import UserPollsForm
from django.contrib.auth.models import User
from auth_login.models import Profile
from django.db import IntegrityError
"""


def login(request):
    template_name = 'login.html'
    data = {}

    auth.logout(request)

    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(
            username=username,
            password=password
        )
        if user is not None:
            # flujo autenticado
            if user.is_active:
                # user valid
                auth.login(request, user)
                return redirect('inventories:index')

            else:
                messages.add_message(
                    request,
                    messages.INFO,
                    'Usuario o Contraseña incorrectos.'
                )
        else:
            messages.add_message(
                request,
                messages.INFO,
                'Usuario o Contraseña incorrectos.'
            )

            # user not valid

    return render(request, template_name, data)


def logout(request):
    auth.logout(request)
    return redirect('auth:login')
