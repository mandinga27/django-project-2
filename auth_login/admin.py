from django.contrib import admin
from auth_login.models import Profile
from django.utils.safestring import mark_safe

# Register your models here.


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass