from django.contrib import admin
from inventories.models import User, Desktop, Laptop, Mobile
from django.utils.safestring import mark_safe

# Register your models here.


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'age',
        'email',
        'cargo',
        'render_photo',
    )

    def render_photo(self, user):
        return mark_safe("<img src='/media/{}' width='80' height='80' />".format(user.photo))


@admin.register(Desktop)
class DesktopAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'price',
        'status',
        'issues',
        'render_photo',
    )

    def render_photo(self, desktop):
        return mark_safe("<img src='/media/{}' width='80' height='80' />".format(desktop.photo))


@admin.register(Laptop)
class LaptopAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'price',
        'status',
        'issues',
        'render_photo',
    )

    def render_photo(self, desktop):
        return mark_safe("<img src='/media/{}' width='80' height='80' />".format(desktop.photo))


@admin.register(Mobile)
class MobileAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'price',
        'status',
        'issues',
        'render_photo',
    )

    def render_photo(self, desktop):
        return mark_safe("<img src='/media/{}' width='80' height='80' />".format(desktop.photo))