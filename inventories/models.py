from django.db import models

# Create your models here.


CARGO = (
    ('SUPERVISOR', 'Supervisor'),
    ('ADMINISTRADOR', 'Administrador'),
)


class User(models.Model):
    name = models.CharField(max_length=150)
    age = models.PositiveIntegerField()
    email = models.EmailField()
    rut = models.CharField(max_length=12)
    cargo = models.CharField(choices=CARGO, max_length=13)
    photo = models.ImageField(upload_to='users/', blank=True)

    def __str__(self):
        return self.name


class Desktop(models.Model):
    name = models.CharField(max_length=200, blank=False)
    price = models.IntegerField()
    photo = models.ImageField(upload_to='devices/desktop', blank=True)

    choices = (
        ('DISPONIBLE', 'Item listo para ser vendido'),
        ('VENDIDO', 'Item fue vendido'),
        ('REABASTECIMIENTO', 'Reposición de ítem')
    )

    status = models.CharField(max_length=16, choices=choices, default='VENDIDO')
    issues = models.CharField(max_length=50, default="Sin detalles")

    def __str__(self):
        return 'Type: {0} Price: {1}'.format(self.name, self.price)


class Laptop(models.Model):
    name = models.CharField(max_length=200, blank=False)
    price = models.IntegerField()
    photo = models.ImageField(upload_to='devices/laptop', blank=True)

    choices = (
        ('DISPONIBLE', 'Item listo para ser vendido'),
        ('VENDIDO', 'Item fue vendido'),
        ('REABASTECIMIENTO', 'Reposición de ítem')
    )

    status = models.CharField(max_length=16, choices=choices, default='VENDIDO')
    issues = models.CharField(max_length=50, default="Sin detalles")

    def __str__(self):
        return 'Type: {0} Price: {1}'.format(self.name, self.price)


class Mobile(models.Model):
    name = models.CharField(max_length=200, blank=False)
    price = models.IntegerField()
    photo = models.ImageField(upload_to='devices/mobile', blank=True)

    choices = (
        ('DISPONIBLE', 'Item listo para ser vendido'),
        ('VENDIDO', 'Item fue vendido'),
        ('REABASTECIMIENTO', 'Reposición de ítem')
    )

    status = models.CharField(max_length=16, choices=choices, default='VENDIDO')
    issues = models.CharField(max_length=50, default="Sin detalles")

    def __str__(self):
        return 'Type: {0} Price: {1}'.format(self.name, self.price)