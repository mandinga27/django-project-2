from django.urls import path

from . import views

app_name = 'inventories'

urlpatterns = [
    path('', views.index, name='index'),
    path('base/', views.base, name='base'),

    path('display/desktop', views.display_desktops, name='display_desktop'),
    path('create/desktop/', views.create_desktop, name="create_desktop"),
    path('update/desktop/<int:id>', views.update_desktop, name='update_desktop'),
    path('delete/desktop/<int:id>', views.delete_desktop, name='delete_desktop'),


    path('display/laptop', views.display_laptops, name='display_laptop'),
    path('create/laptop/', views.create_laptop, name="create_laptop"),
    path('update/laptop/<int:id>', views.update_laptop, name='update_laptop'),
    path('delete/laptop/<int:id>', views.delete_laptop, name='delete_laptop'),

    path('display/mobile', views.display_mobiles, name='display_mobile'),
    path('create/mobile/', views.create_mobile, name="create_mobile"),
    path('update/mobile/<int:id>', views.update_mobile, name='update_mobile'),
    path('delete/mobile/<int:id>', views.delete_mobile, name='delete_mobile'),


    path('display/user', views.display_users, name='display_user'),
    path('create/user/', views.create_user, name="create_user"),
    path('update/user/<int:id>', views.update_user, name='update_user'),
    path('delete/user/<int:id>', views.delete_user, name='delete_user'),

]

