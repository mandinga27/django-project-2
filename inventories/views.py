from django.shortcuts import render, redirect
from django.core.paginator import (Paginator,
                                   EmptyPage,
                                   PageNotAnInteger)
from django.contrib.auth.decorators import (login_required,
                                            permission_required)
from inventories.models import (Desktop,
                                Laptop,
                                Mobile,
                                User)
from inventories.forms import (DesktopForm,
                               LaptopForm,
                               MobileForm,
                               UserForm)

# Create your views here.


@login_required()
def index(request):
    data = {}
    template_name = 'index.html'
    data['items'] = Laptop.objects.all()
    context = {
        'items': data,
        'header': 'Laptops'
    }

    return render(request, template_name, data)


@login_required()
def base(request):
    data = {}
    template_name = 'base.html'

    data['title'] = "Desde la Base"

    return render(request, template_name)


@login_required()
def display_users(request):
    data = {}
    template_name = 'display_users.html'
    data['items'] = User.objects.all()
    context = {
        'items': data,
        'header': 'Users'
    }

    return render(request, template_name, data)


@permission_required('inventories.add_user')
def create_user(request):
    data = {}
    template_name = 'create_user.html'

    data['form'] = UserForm(request.POST or None)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('inventories:display_user')

    return render(request, template_name, data)


@permission_required('inventories.change_user')
def update_user(request, id):
    template_name = 'update_user.html'
    data = {}

    user = User.objects.get(pk=id)
    data['form'] = UserForm(request.POST or None, instance=user)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('inventories:display_user')

    return render(request, template_name, data)


@permission_required('inventories.delete_user')
def delete_user(request, id):
    #data = {}
    user = User.objects.get(pk=id)
    #data['desktop'] = Desktop.objects.get(pk=id)
    #template_name = 'update_mobile.html'
    user.delete()
    #return render(request, template_name, data)
    return redirect('inventories:display_user')


@login_required()
def display_laptops(request):
    data = {}
    template_name = 'display_laptops.html'
    data['query'] = ''

    if 'l' in request.GET:
        # caso de busqueda
        data['query'] = request.GET.get('l')
        laptop_lists = Laptop.objects.filter(question_text__icontains=data['query'])

    else:
        # caso normal
        laptop_lists = Laptop.objects.all()

    page = request.GET.get('page', 1)
    paginator = Paginator(laptop_lists, 4)

    try:
        data['laptops'] = paginator.page(page)
    except PageNotAnInteger:
        data['laptops'] = paginator.page(1)
    except EmptyPage:
        data['laptops'] = paginator.page(paginator.num_pages)

    return render(request, template_name, data)


@login_required()
def display_desktops(request):
    data = {}
    template_name = 'display_desktops.html'

    data['query'] = ''
    if 'd' in request.GET:
        # caso de busqueda
        data['query'] = request.GET.get('d')
        desktop_lists = Desktop.objects.filter(question_text__icontains=data['query'])

    else:
        # caso normal
        desktop_lists = Desktop.objects.all()

    page = request.GET.get('page', 1)
    paginator = Paginator(desktop_lists, 4)

    try:
        data['desktops'] = paginator.page(page)
    except PageNotAnInteger:
        data['desktops'] = paginator.page(1)
    except EmptyPage:
        data['desktops'] = paginator.page(paginator.num_pages)

    return render(request, template_name, data)


@login_required()
def display_mobiles(request):
    data = {}
    template_name = 'display_mobiles.html'

    data['query'] = ''

    if 'm' in request.GET:
        # caso de busqueda
        data['query'] = request.GET.get('m')
        mobile_lists = Mobile.objects.filter(question_text__icontains=data['query'])

    else:
        # caso normal
        mobile_lists = Mobile.objects.all()

    page = request.GET.get('page', 1)
    paginator = Paginator(mobile_lists, 4)

    try:
        data['mobiles'] = paginator.page(page)
    except PageNotAnInteger:
        data['mobiles'] = paginator.page(1)
    except EmptyPage:
        data['mobiles'] = paginator.page(paginator.num_pages)

    return render(request, template_name, data)


@permission_required('inventories.add_desktop')
def create_desktop(request):
    data = {}
    template_name = 'create_desktop.html'

    data['form'] = DesktopForm(request.POST or None, request.FILES)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('inventories:display_desktop')

    return render(request, template_name, data)


@permission_required('inventories.add_laptop')
def create_laptop(request):
    data = {}
    template_name = 'create_laptop.html'

    data['form'] = LaptopForm(request.POST or None, request.FILES)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('inventories:display_laptop')

    return render(request, template_name, data)


@permission_required('inventories.add_mobile')
def create_mobile(request):
    data = {}
    template_name = 'create_mobile.html'

    data['form'] = MobileForm(request.POST or None, request.FILES)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('inventories:display_mobile')

    return render(request, template_name, data)


@permission_required('inventories.change_desktop')
def update_desktop(request, id):
    template_name = 'update_desktop.html'
    data = {}

    desktop = Desktop.objects.get(pk=id)
    data['form'] = DesktopForm(request.POST or None, instance=desktop)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('inventories:display_desktop')

    return render(request, template_name, data)


@permission_required('inventories.change_laptop')
def update_laptop(request, id):
    template_name = 'update_laptop.html'
    data = {}

    laptop = Laptop.objects.get(pk=id)
    data['form'] = LaptopForm(request.POST or None, instance=laptop)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('inventories:display_laptop')

    return render(request, template_name, data)


@permission_required('inventories.change_mobile')
def update_mobile(request, id):
    template_name = 'update_mobile.html'
    data = {}

    mobile = Mobile.objects.get(pk=id)
    data['form'] = MobileForm(request.POST or None, instance=mobile)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('inventories:display_mobile')

    return render(request, template_name, data)


@permission_required('inventories.delete_desktop')
def delete_desktop(request, id):
    # data = {}
    desktop = Desktop.objects.get(pk=id)
    # data['desktop'] = Desktop.objects.get(pk=id)
    # template_name = 'update_mobile.html'
    desktop.delete()
    # return render(request, template_name, data)
    return redirect('inventories:display_desktop')


@permission_required('inventories.delete_laptop')
def delete_laptop(request, id):
    # data = {}
    laptop = Laptop.objects.get(pk=id)
    # data['desktop'] = Desktop.objects.get(pk=id)
    # template_name = 'update_mobile.html'
    laptop.delete()
    # return render(request, template_name, data)
    return redirect('inventories:display_laptop')


@permission_required('inventories.delete_mobile')
def delete_mobile(request, id):
    # data = {}
    mobile = Mobile.objects.get(pk=id)
    # data['desktop'] = Desktop.objects.get(pk=id)
    # template_name = 'update_mobile.html'
    mobile.delete()
    # return render(request, template_name, data)
    return redirect('inventories:display_mobile')