from django import forms
from inventories.models import Desktop, Laptop, Mobile, User


class DesktopForm(forms.ModelForm):
    class Meta:
        model = Desktop
        fields = '__all__'


class LaptopForm(forms.ModelForm):
    class Meta:
        model = Laptop
        fields = '__all__'


class MobileForm(forms.ModelForm):
    class Meta:
        model = Mobile
        fields = '__all__'


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields ='__all__'